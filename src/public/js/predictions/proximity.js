(function (window) {
    const setProximity = (target, distance) => {
        let el = document.querySelector('.proximity-indicator.' + target);

        if (distance < 2) {
            el.classList.remove('warning');
            el.classList.add('danger');
        } else if (distance < 5) {
            el.classList.remove('danger');
            el.classList.add('warning');
        } else {
            el.classList.remove('warning');
            el.classList.remove('danger');
        }

        if (el.querySelector('p')) {
            el.querySelector('p').innerText = distance + 'm';
        }

        switch (true) {
            case el.classList.contains('front-left'):
                el.style.left = 15 + (50 / Math.max(5, distance)) + '%';
                el.style.transform = 'scale(' + Math.max(50, distance) + '%)';
                break;
            case el.classList.contains('front-right'):
                el.style.right = 15 + (50 / Math.max(5, distance)) + '%';
                el.style.transform = 'rotate(90deg) scale(' + Math.max(50, distance) + '%)';
                break;
            case el.classList.contains('rear-left'):
                el.style.left = 15 + (50 / Math.max(5, distance)) + '%';
                el.style.transform = 'rotate(270deg) scale(' + Math.max(50, distance) + '%)';
                break;
            case el.classList.contains('rear-right'):
                el.style.right = 13 + (50 / Math.max(5, distance)) + '%';
                el.style.transform = 'rotate(180deg) scale(' + Math.max(50, distance) + '%)';
        }
    }

    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    const detectCollision = function (left, right) {
        if (left) {
            document.querySelector('.cmm-content').classList.add('left-collision');
        } else {
            document.querySelector('.cmm-content').classList.remove('left-collision');
        }

        if (right) {
            document.querySelector('.cmm-content').classList.add('right-collision');
        } else {
            document.querySelector('.cmm-content').classList.remove('right-collision');
        }

        if (left || right) {
            document.querySelector('.cmm-content-footer').classList.add('active');
        } else {
            document.querySelector('.cmm-content-footer').classList.remove('active');
        }
    }

    const userWantsToChangeLane = (wants) => {
        let content = document.querySelector('.cmm-content');
        let footer = document.querySelector('.cmm-content-footer');
        let legend = document.querySelector('.legends-container');

        if (wants) {
            content.classList.add('danger');
            footer.classList.add('danger');
        } else {
            content.classList.remove('danger');
            footer.classList.remove('danger');
        }
    }

    const demo = function () {
        setInterval(function () {
            setProximity('front-left', getRandomInt(1, 10));
            setProximity('front-right', getRandomInt(1, 10));
            setProximity('rear-left', getRandomInt(1, 10));
            setProximity('rear-right', getRandomInt(1, 10));

            detectCollision(getRandomInt(0, 50) < 10, getRandomInt(0, 50) < 10);
        }, 1000);
    }
    window.setProximity = setProximity;
    window.detectCollision = detectCollision;
    window.userWantsToChangeLane = userWantsToChangeLane;
    window.demo = demo;
})(window);
