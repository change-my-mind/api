const express = require('express');
const app = express();
const http = require('http');
var multer = require('multer');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const { Notion } = require("@neurosity/notion");
const { readdir } = require('fs/promises');
const fs = require('fs');
const csv = require("csvtojson");
const bodyParser = require('body-parser');
require("dotenv").config();

app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded())
app.use(bodyParser.json({ limit: '1024mb' }))

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, req.dirname);
    },
    filename: function (req, file, callback) {
        callback(null, req.filename);
    }
});

var upload = multer({ storage : storage }).array('csv', 100);
var uploadVideo = multer({ storage : storage }).array('video', 100);

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/views/index.html');
});

app.post('/upload',function (req,res) {
    var id = new Date().toISOString()
        .replace(':', '')
        .replace(':', '')
        .replace(':', '');
    var dirname = __dirname + '/public/experiments/' + id
    fs.mkdirSync(dirname)

    req.dirname = dirname;
    req.filename = 'input.csv';

    upload(req, res, function (err) {
        if (err) {
            return res.end("Error uploading file.");
        }

        res.end(id);
    });
});

app.post('/upload-video',function (req,res) {
    var dirname = __dirname + '/public/experiments/' + req.query.id

    req.dirname = dirname;
    req.filename = 'video.mp4';

    uploadVideo(req, res, function (err) {
        console.log(err)
        if (err) {
            return res.end("Error uploading file.");
        }

        res.end(dirname);
    });
});

app.get('/predictions', (req, res) => {
    res.sendFile(__dirname + '/views/predictions.html');
});

app.get('/hc', (req, res) => {
    res.send('ok')
})

io.on('connection', async (socket) => {
    console.log('a user connected');
    const notion = new Notion({ deviceId: process.env.DEVICE_ID });
    await notion
        .login({
            email: process.env.EMAIL,
            password: process.env.PASSWORD,
        })
        .catch((error) => {
            console.log(error);
            throw new Error(error);
        });
    console.log('notion initialized')

    notion.calm().subscribe((calm) => {
        io.emit('calm', calm.probability);
    });

    notion
        .predictions("leftArm")
        .subscribe(prediction => {
            console.log(prediction)
            io.emit('leftArm', prediction)
        });


    notion.signalQuality().subscribe(signalQuality => {
        io.emit('signalQuality', signalQuality);
    });

    notion.brainwaves("raw").subscribe((brainwaves) => {
        io.emit('raw', brainwaves);
    });

    notion.brainwaves("psd").subscribe((brainwaves) => {
        io.emit('psd', brainwaves);
    });
});

app.get('/experiments', async (req, res) => {
    const getDirectories = async source => (await readdir(source, { withFileTypes: true }))
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name)

    let dirs = await getDirectories(__dirname + '/public/experiments/');
    let result = []
    dirs.forEach(function (dir) {
        var completed = true;

        ['output.csv'].forEach(file => {
            if (completed && !fs.existsSync(__dirname + '/public/experiments/' + dir + '/' + file)) {
                completed = false;
            }
        })

        result.push({
            name: dir,
            completed,
        })
    });
    res.send(result)
})

app.get('/experiments/:experimentId', (req, res) => {
    csv()
        .fromFile(__dirname + '/public/experiments/' + req.params.experimentId + '/output.csv')
        .then(function(jsonArrayObj){ //when parse finished, result will be emitted here.
            res.send(jsonArrayObj)
        })
});

app.post('/save-predictions', (req, res) => {
    fs.writeFileSync(
        __dirname + '/public/experiments/' + req.body.experiment + '/output.csv',
        req.body.content
    );
    res.send('ok')
});

server.listen(3000, () => console.log('App is running on :3000'))
