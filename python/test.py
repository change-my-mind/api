import pandas as pd
import matplotlib.pyplot as plt
# from scipy.interpolate import spline

file = pd.read_csv('sources/Group_349_1449.csv')
data = []
# for sensor in range(4):
#     for arg in range(10):
#         data[str(sensor) + '_' + str(arg)] = file[f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{arg}_._m_dy']

# plt.plot(data['0_0']/128, data['0_1']/128)
# plt.show()

for sensor in range(4):
    df = file[[f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{x}_._m_dy' for x in range(1)]].head(1800)
    data.append(df.mean(axis=1))

# plot lines
for i in range(4):
    plotdata = None

    if (i % 2 == 0):
        plotdata = (data[i] / 128).replace(0, 15)
    else:
        plotdata = (data[i] / 128).replace(0, -15)

    if plotdata is not None:
        plt.plot(plotdata, label = f"line{i}")
plt.legend()
plt.show()