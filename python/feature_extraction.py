import pandas as pd


class FeatureExtraction:
    def __init__(self, source, direction):
        self.copy = None
        self.direction = direction
        self.source = pd.read_csv(source)

        features = {}
        for sensor in range(4):
            if self.direction == 'left' and sensor % 2:
                continue
            if self.direction == 'right' and not (sensor % 2):
                continue
            for id in range(10):
                features[
                    f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{id}_._m_dx'] = f'dx_{"front" if sensor in [0, 1] else "back"}_{id}'
                features[
                    f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{id}_._m_dy'] = f'dy_{"front" if sensor in [0, 1] else "back"}_{id}'
                features[
                    f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{id}_._m_vx'] = f'vx_{"front" if sensor in [0, 1] else "back"}_{id}'
                features[
                    f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{id}_._m_vy'] = f'vy_{"front" if sensor in [0, 1] else "back"}_{id}'
                features[
                    f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{id}_._m_ax'] = f'ax_{"front" if sensor in [0, 1] else "back"}_{id}'
                features[
                    f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{sensor}_._m_objects._m_value._{id}_._m_ay'] = f'ay_{"front" if sensor in [0, 1] else "back"}_{id}'

        self.source = self.source[features.keys()]
        self.source = self.source.rename(columns=features)

        for feature in features.values():
            if 'd' in feature:
                self.source[feature] /= 128
            elif 'v' in feature:
                self.source[feature] /= 256
            else:
                self.source[feature] /= 2048

    def normalized(self):
        return self.copy

    def extract_data(self):
        print(f"Returning extracted columns for {self.direction} side.")
        return self.source

