import pandas as pd

left = pd.read_csv('outputs_left/training.csv')
right = pd.read_csv('outputs_right/training.csv')
left_block = pd.read_csv('outputs_left/target.csv')
right_block = pd.read_csv('outputs_right/target.csv')

left.columns.values[0] = 'left_front'
left.columns.values[1] = 'left_back'

right.columns.values[0] = 'right_front'
right.columns.values[1] = 'right_back'

left_block.columns.values[0] = 'left_block'
right_block.columns.values[0] = 'right_block'

joined = left.join([right, left_block, right_block])

joined['left_front'] = joined['left_front'] / 128
joined['left_back'] = joined['left_back'] / 128
joined['right_front'] = joined['right_front'] / -128
joined['right_back'] = joined['right_back'] / -128

joined.to_csv('dummy_data_xy.csv', index=False)