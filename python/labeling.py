import pandas as pd
import numpy as np
import math


class Labeling:
    def __init__(self, source, labels, direction):
        self.source = pd.read_csv('sources/' + source)
        self.labels = pd.read_csv('sources/' + labels)
        self.direction = direction
        self.source_distance = self.source

        if direction == 'left':
            self.source_y = self.source[[
                                            f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{x}_._m_objects._m_value._0_._m_dy'
                                            for x in [0, 2]] + ['t']]
            self.source_x = self.source[[
                                            f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{x}_._m_objects._m_value._0_._m_dx'
                                            for x in [0, 2]] + ['t']]
        else:
            self.source_y = self.source[[
                                            f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{x}_._m_objects._m_value._0_._m_dy'
                                            for x in [1, 3]] + ['t']]
            self.source_x = self.source[[
                                            f'_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._{x}_._m_objects._m_value._0_._m_dx'
                                            for x in [1, 3]] + ['t']]

        self.source_y.columns.values[0] = 'front'
        self.source_x.columns.values[0] = 'front'
        self.source_y.columns.values[1] = 'back'
        self.source_x.columns.values[1] = 'back'
        if direction == 'left':
            self.source_distance['front'] = self.source_y['front'] * math.cos(-42) + self.source_x['front'] * math.sin(
                -42) + 0.6286
            self.source_distance['back'] = self.source_y['back'] * math.cos(-135) + self.source_x['back'] * math.sin(
                -135) + 0.738
        else:
            self.source_distance['front'] = self.source_y['front'] * math.cos(42) + self.source_x['front'] * math.sin(
                42) - 0.6286
            self.source_distance['back'] = self.source_y['back'] * math.cos(135) + self.source_x['back'] * math.sin(
                135) + 0.738

        self.target = self.generate_target()
        self.training_data = self.source_distance[['front', 'back']]

        self.training_data.to_csv(f'outputs_{direction}/training.csv', index=False)
        pd.DataFrame(np.asarray(self.target)).to_csv(f'outputs_{direction}/target.csv', index=False)

    def generate_target(self):
        target = []
        start = self.labels['from']
        end = self.labels['to']
        for t in self.source['t']:
            for s, e in zip(start, end):
                if t > e:
                    continue
                if s <= t <= e:
                    target.append(0)
                    break
                else:
                    target.append(1)
                    break
            else:
                target.append(1)

        return target


if __name__ == '__main__':
    trying = Labeling('Group_349_1449.csv', 'right_1449.csv', 'right')
    trying = Labeling('Group_349_1449.csv', 'left_1449.csv', 'left')

