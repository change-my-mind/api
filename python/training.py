import numpy as np
from keras import Sequential
from keras.layers import Dense, Conv1D, MaxPooling1D, Flatten
from feature_extraction import FeatureExtraction
from sklearn.model_selection import train_test_split


def train(direction):
    X = FeatureExtraction('sources/Group_349_1449.csv', direction).extract_data()
    X = X.to_numpy()
    y = np.loadtxt(f'outputs_{direction}/target.csv', delimiter=',', skiprows=1)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1)

    # reshaping
    X_train = X_train.reshape(-1, 120, 1).astype('float32')
    X_test = X_test.reshape(-1, 120, 1).astype('float32')

    model = Sequential()
    model.add(Conv1D(filters=32, kernel_size=5, input_shape=(120, 1)))
    model.add(MaxPooling1D(pool_size=10))
    model.add(Flatten())
    model.add(Dense(1000, activation='relu'))
    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer='Adam', loss='binary_crossentropy', metrics='accuracy')
    model.fit(X_train, y_train)

    score, acc = model.evaluate(X_test, y_test)
    print(f'score: {score}')
    print(f'acc: {acc}')

    model.save(f'model_{direction}')


train('left')
train('right')
