import pandas as pd
import matplotlib.pyplot as plt

# elozes =  pd.read_csv('elozes.csv')
data = pd.read_csv('sources/Group_349_1449.csv')

df1 = data['_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._0_._m_objects._m_value._0_._m_dy']
df2 = data['_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._1_._m_objects._m_value._0_._m_dy']
df3 = data['_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._2_._m_objects._m_value._0_._m_dy']
df4 = data['_g_Infrastructure_CCR_NET_NetRunnablesClass_m_rteInputData_out_local.TChangeableMemPool._._._m_arrayPool._0_._elem._m_cornerData._m_value._3_._m_objects._m_value._0_._m_dy']


plt.plot(df1, label="left_front")
plt.plot(df2, label="right_front")

plt.plot(df3, label="left_back")
plt.plot(df4, label="right_back")

plt.legend()
plt.show()

# df['new'] = pd.Series([0 for x in range(len(df.index))])


# for x in elozes:
#     if elozes['side'] == 'r':
