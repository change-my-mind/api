#!/bin/sh

BASEDIR=/opt/experiments/*/

ls -la /opt/experiments

for dir in $BASEDIR
do
    dir=${dir%*/}      # remove the trailing "/"

    if test -f "$dir/output.csv"; then
      echo "Skipping $dir, output already exists"
      continue
    fi

    echo "Calculating $dir"
    python scoring.py "$dir/input.csv" "$dir/output.csv"
done
