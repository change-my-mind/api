from tensorflow import keras
from feature_extraction import FeatureExtraction
import numpy as np
import pandas as pd
import sys


class Scoring:
    def __init__(self, source, target):
        self.left = FeatureExtraction(source, 'left').extract_data()
        self.right = FeatureExtraction(source, 'right').extract_data()

        self.left_np = self.left.to_numpy().reshape(-1, 120, 1).astype('float32')
        self.right_np = self.right.to_numpy().reshape(-1, 120, 1).astype('float32')

        self.model_left = keras.models.load_model('/app/model_left')
        self.model_right = keras.models.load_model('/app/model_right')

        self.target = target

    def score(self):
        predicted_left = self.model_left.predict(self.left_np)
        predicted_right = self.model_right.predict(self.right_np)

        return predicted_left, predicted_right

    def make_csv(self):
        rename_left = {"dy_front_0": "left_front",
                       "dy_back_0": "left_back"}
        rename_right = {"dy_front_0": "right_front",
                        "dy_back_0": "right_back"}
        self.left = self.left[['dy_front_0', 'dy_back_0']].rename(columns=rename_left)
        self.right = self.right[['dy_front_0', 'dy_back_0']].rename(columns=rename_right)
        l, r = self.score()
        pred_left = pd.DataFrame(l, columns=['left_block']).round().astype(int)
        pred_right = pd.DataFrame(r, columns=['right_block']).round().astype(int)

        output = self.left.join([self.right, pred_left, pred_right])

        output.to_csv(self.target)


if __name__ == '__main__':
    path_to_csv = sys.argv[1]
    target = sys.argv[2]
    Scoring(path_to_csv, target).make_csv()
