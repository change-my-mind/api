FROM node:14.19-alpine

WORKDIR /app

COPY package.json ./
RUN npm install && npm install -g nodemon

COPY ./src ./src
EXPOSE 3000
CMD [ "nodemon" ]
